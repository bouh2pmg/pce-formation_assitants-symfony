﻿
Quick tutorial

**Symfony 3**

![image alt text](image_0.png)
![image alt text](image_1.png)

## Introduction

The purpose of this quick tutorial is to give you an overview of what Symfony is and how to use it. We assume that you have the basic object programming knowledge so everything will not be explained. 

**Symfony** is a very popular PHP framework used by big company like Dailymotion Yahoo!..etc. It’s fast, flexible and has a lot of documentation. The good thing is that you can use certain pieces of Symfony to build your app, you don’t need to use the framework in its entirety.

## Prerequisites

First make sure you have installed all of theses tools : 

* Download/install/configure **Symfony** following this link : [https://symfony.com/doc/3.3/index.html](https://symfony.com/doc/3.3/index.html)

* Download/install/configure **Composer **following this link : [https://getcomposer.org/download/](https://getcomposer.org/download/) (package manager for PHP, same as NPM)

Create a new Symfony project, update it, and launch it :

    $> symfony new project_name
    $> cd project_name
    $> composer update //make sure to have latest dependency
    $> php bin/console server:start //launch the dev server on localhost:8000


Now if everything is working well, you should get a nice home page like this :
![image alt text](image_2.png)

You are ready to start !

Side note : This tutorial has been tested on Linux (Fedora) only.


## What are we gonna do ?

The best way to learn a programming language is by using it.. so we are going to build a **Contact Form.** We will cover many things that you would normally use when working with Symfony:

* Controllers

* Forms

* Templates

* Routing

* Development environment (app_dev)

# Get started

Be sure to read the **project structure doc** before starting : [http://symfony.com/doc/current/quick_tour/the_architecture.html](http://symfony.com/doc/current/quick_tour/the_architecture.html)

You’ll have to follow this tutorial :  [https://www.codereviewvideos.com/course/symfony-3-for-beginners](https://www.codereviewvideos.com/course/symfony-3-for-beginners)

This is an existing tutorial, it’s well made and everything is very well explained, a video is available for each section of the tutorial. It has been tested and the source is available at this 
git address : `git@gitlab.com:webac/formation/SYMFONY-StarterPack.git`



